from bs4 import BeautifulSoup
import pymysql
import requests
import time
from lxml import etree
import re
import urllib.request
import os


def connect_db():
    connect = pymysql.connect(
        user="root",
        password="123456",
        host="localhost",
        db="movie_db",
        port=3306,
        charset=("utf8"),
        use_unicode=True,
    )
    return connect


def get_html_2(web_url):
    header = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36"}
    html = requests.get(url=web_url, headers=header).text
    Soup = BeautifulSoup(html, "lxml")
    data = Soup.find("ol").find_all(class_='pic')
    for item in data:
        nums = re.findall(r'<em class="">\d+</em>', str(item), re.S | re.M)
        nums = re.findall(r'\d+', str(nums), re.S | re.M)
        img_lebel = item.find('a')
        img_url = img_lebel['href']
        print(nums[0], ":", img_url)
        second_html = requests.get(url=img_url, headers=header).text
        f = etree.HTML(second_html)
        name = f.xpath('//*[@id="content"]/h1/span[1]/text()')[0]
        print('name:', name)
        pic = f.xpath('//*[@id="mainpic"]/a/img/@src')[0]
        print('pic:', pic)
        # pic_id = pic.replace('https://img9.doubanio.com/view/photo/s_ratio_poster/public/','')
        urllib.request.urlretrieve(pic, 'D://Projects/djangoProject/movie_picture/%s.jpg' % nums[0])
        length = f.xpath('//*[@id="info"]/span[@property="v:runtime"]/@content')[0]
        print('length:', length)
        grade = f.xpath('//*[@id="interest_sectl"]/div[1]/div[2]/strong/text()')[0]
        print('grade:', grade)
        time.sleep(1)
    return


if __name__ == "__main__":
    connect = connect_db()
    cursor = connect.cursor()
    page = 226
    while page <= 250:
        web_url = "https://movie.douban.com/top250?start=%s&filter=" % page
        all_move = get_html_2(web_url)
        page += 25
        time.sleep(3)
