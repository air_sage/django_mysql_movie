from rest_framework import serializers
from movie_app.models import Movie
from rest_framework.validators import UniqueTogetherValidator

# 序列器这部分内容在未来像拓展前端页面表单的传输时会用到
class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'
