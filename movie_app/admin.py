from django.contrib import admin

# Register your models here.
from movie_app.models import Movie

admin.site.register(Movie)
