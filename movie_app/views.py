from django.shortcuts import render
from movie_app.models import Movie
from rest_framework import viewsets
from movie_app.serializers import MovieSerializer
from .models import Movie


# Create your views here.
class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

# /about页面的调用
def about(request):
    return render(request, 'about.html')

# /about/movies页面的调用
def index(request):
    movies = Movie.objects.all()
    return render(request, 'movie_index.html', {'movies': movies})
