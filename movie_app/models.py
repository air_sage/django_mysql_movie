from django.db import models


# Create your models here.
# 在爬取数据之前先确定模型,并且通过数据迁移在mysql中建立好对应的空table
class Movie(models.Model):
    pic = models.CharField(max_length=100, help_text='picture URL of the movie')
    name = models.CharField(max_length=70, help_text='name of the movie')
    length = models.CharField(max_length=30, help_text='length of the movie')
    grade = models.CharField(max_length=30, help_text='grade of the movie')
