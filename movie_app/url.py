from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views
from django.conf.urls import url

router = DefaultRouter()
router.register(r'movie', views.MovieViewSet, basename="movie")

urlpatterns = [
    path('', include(router.urls)),
    url(r'about/movies', views.index),
    url(r'about', views.about)
    # url(r'^movie_picture/(?P<path>.*)', 'django.views.static.serve',
    #      {'document_root': 'D://Projects/djangoProject/movie_picture'}),
]
