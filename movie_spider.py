from bs4 import BeautifulSoup
import pymysql
import requests
import time
from lxml import etree
import re
import urllib.request
import os

# 连接数据库
def connect_db():
    connect = pymysql.connect(
        user="root",
        password="123456",
        host="localhost",
        db="movie_db",
        port=3306,
        charset=("utf8"),
        use_unicode=True,
    )
    return connect

# 爬取数据
def get_html_2(web_url):
    # 填写请求头，伪装成浏览器
    header = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36"}
    # 将html文件转成文本
    html = requests.get(url=web_url, headers=header).text
    # 使用BeautifulSoup解析html
    Soup = BeautifulSoup(html, "lxml")
    # 找出ol标签下的所有class='pic'的标签，这里面有我们想要的信息——进入二级页面的网址
    data = Soup.find("ol").find_all(class_='pic')
    for item in data:
        # 找出每部电影的排名
        nums = re.findall(r'<em class="">\d+</em>', str(item), re.S | re.M)
        nums = re.findall(r'\d+', str(nums), re.S | re.M)
        # 在一级页面里面的图片点击可以进入相应的二级页面，内容在href属性里面
        img_lebel = item.find('a')
        img_url = img_lebel['href']
        # 由于nums依旧是一个列表，只不过只有一个元素，所以我们取出这个元素，显示效果就等于去除了中括号[]
        print(nums[0], ":", img_url)
        # 通过img_url进入二级页面，里面有电影的详细信息，比如以及页面上看不到的播放时长length
        second_html = requests.get(url=img_url, headers=header).text
        # 由于所要找的标签没有显著的特征，所以我决定用xpath的方式来查找，xpath的具体表达式可以在chrome浏览器的检查里面找到标签右键copy
        f = etree.HTML(second_html)
        # 找到片名
        name = f.xpath('//*[@id="content"]/h1/span[1]/text()')[0]
        print('name:', name)
        # 找到图片url
        pic = f.xpath('//*[@id="mainpic"]/a/img/@src')[0]
        print('pic:', pic)
        # 下面这一句是下载图片，由于下载图片时间过长，所以最好分开下载，在movie_pic_spider.py文件中可以单独爬取图片
        # urllib.request.urlretrieve(pic, 'D://Projects/djangoProject/movie_picture/%s.jpg' % nums[0])
        # 找到播放时长
        length = f.xpath('//*[@id="info"]/span[@property="v:runtime"]/@content')[0]
        print('length:', length)
        # 找到评分
        grade = f.xpath('//*[@id="interest_sectl"]/div[1]/div[2]/strong/text()')[0]
        print('grade:', grade)
        # 把信息打包字典格式的数据,逐条把数据存入mysql数据库
        movie_data = {'name': name, 'pic': pic, 'length': length, 'grade': grade}
        cursor.execute("insert into movie_app_movie(name,pic,length,grade)values(%s,%s,%s,%s)",
                       [movie_data['name'], movie_data['pic'], movie_data['length'], movie_data['grade']])
        # 提交
        connect.commit()
        # 停顿一秒,因为爬取速度太快会被识别为非浏览器操作,进而导致被反爬虫--我就被IP限制过
        # 特别是在爬取图片的时候最容易发生这种情况,这也是我为什么把图片下载单独一个文件的原因之一
        # 建议大家准备多几个ip,尽量避免用自己的本地ip,因为我也不知道一封要封多久,可能是一天,也可能是永久
        time.sleep(1)
    # print(data)
    return

# 主函数
if __name__ == "__main__":
    # 调用数据库,设置游标
    connect = connect_db()
    cursor = connect.cursor()
    # 把页面设置为零,其实这是卡片序号,在爬取图片的时候遇到反扒错图停止了,可以换完ip改动这个数字接着爬
    page = 0
    while page <= 250:
        # 要爬取的网址:豆瓣top250
        web_url = "https://movie.douban.com/top250?start=%s&filter=" % page
        all_move = get_html_2(web_url)
        # 因为豆瓣一页放25条数据,所以每一页爬完,加25
        page += 25
        time.sleep(3)
    # 关闭数据库
    connect.close()
