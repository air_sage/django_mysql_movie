Django + MySql + 爬虫
===========================

##### 环境依赖
Django v2.2.3
PyMySql v0.9.3

`$pip install bs4`

`$pip install lxml`

`$pip install mysqlclient`

##### 部署步骤

1. 创建数据库
	打开MySql Workbench创建数据库

2. 连接数据库
	在settings.py中配置数据库
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'movie_db',  # 这里不要用绝对路径
        'USER': 'root',
        'PASSWORD': '123456',
        # 数据库的主机地址
        'HOST': '127.0.0.1',
        # 数据库的端口号，默认为3306,也可以不写这一行代码
        'PORT': '3306'
    }
}
```

3. python manage.py makemigrations  //数据迁移

4. python manage.py migrate


4. python manage.py runserver 127.0.0.1:8080  //启动服务

##### 目录结构描述
```
djangoProject(外层)
│  manage.py				//启动文件
│  movie_pic_spider.py		      //图片爬虫文件
│  movie_spider.py			    //电影信息爬虫文件
│  README.md			      // help
│
├─.idea
│  │  .gitignore
│  │  djangoProject.iml
│  │  misc.xml
│  │  modules.xml
│  │  workspace.xml
│  │
│  └─inspectionProfiles
│          profiles_settings.xml
│          Project_Default.xml
│
├─djangoProject
│  │  settings.py				//配置文件
│  │  urls.py				       //路由文件
│  │  wsgi.py
│  │  __init__.py
│  │
│  └─__pycache__
│          settings.cpython-35.pyc
│          urls.cpython-35.pyc
│          wsgi.cpython-35.pyc
│          __init__.cpython-35.pyc
│
├─movie_app
│  │  admin.py				   //管理文件
│  │  apps.py
│  │  models.py				  //模型文件
│  │  serializers.py			 // 序列器文件
│  │  tests.py
│  │  url.py				  // app路由文件
│  │  views.py				  // 视图文件
│  │  __init__.py
│  │
│  ├─migrations				//数据迁移文件
│  │  │  0001_initial.py
│  │  │  0002_auto_20201105_1638.py
│  │  │  0003_auto_20201105_1639.py
│  │  │  __init__.py
│  │  │
│  │  └─__pycache__
│  │          0001_initial.cpython-35.pyc
│  │          0002_auto_20201105_1638.cpython-35.pyc
│  │          0003_auto_20201105_1639.cpython-35.pyc
│  │          __init__.cpython-35.pyc
│  │
│  ├─templates				//模板文件
│  │      about.html			//about页面
│  │      base.html				//Bootstrap模板
│  │      movie_index.html		 //about/movies页面
│  │
│  └─__pycache__
│          admin.cpython-35.pyc
│          models.cpython-35.pyc
│          serializers.cpython-35.pyc
│          url.cpython-35.pyc
│          views.cpython-35.pyc
│          __init__.cpython-35.pyc
│
├─movie_picture				//图片存放文件夹
│      1.jpg
│	... ...
│      250.jpg
```

##### V1.0.0 版本内容更新(20201106-1123)

1. 爬虫	 爬取豆瓣电影图片和信息
2. mysql	把信息存入数据库
3. django	 在自建网站上面显示电影信息

##### 效果图

![image-20201106113046927](README.assets/image-20201106113046927.png)

